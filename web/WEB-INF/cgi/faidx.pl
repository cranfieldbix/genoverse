#!/usr/bin/perl

use strict;
use Data::Dumper;
use CGI;

#chdir('/');
my $q = CGI->new;

print "Content-type: text\n\n";
$ENV{"PATH"} = "/usr/bin";

# The | tail -n +2 skips the fasta header returned by faidx
system("/usr/bin/samtools faidx ".$q->param('file')." ".$q->param('r')." | tail -n +2") == 0 or die "system failed: $?, $!";
