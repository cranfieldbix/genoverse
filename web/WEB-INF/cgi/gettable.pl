#!/usr/bin/perl

use strict;
use Data::Dumper;
use CGI;

my $q = CGI->new;

print "Content-type: html\n\n";
# The Rscript which generates the marker table relies on package xtable
system("tabix ".$q->param('file')." chr".$q->param('chrnum')." | cut -f2,4,5 | Rscript tohtml.R ".$q->param('chrnum')) == 0 or die "system failed: $?, $!";
