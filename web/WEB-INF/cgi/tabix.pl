#!/usr/bin/perl

use strict;
use Data::Dumper;
use CGI;

#chdir('/');
my $q = CGI->new;

print "Content-type: text\n\n";
$ENV{"PATH"} = "/usr/bin";
system("/usr/local/sbin/tabix", $q->param('file'), $q->param('r')) == 0 or die "system failed: $?, $!";
