var genoverseConfig = {
    container: '#genoverse',
    width: 1366,
    // chromosomeSize : 249250621, // chromosome 1, human
     chr            : 1,
//                 start          : 45476751,
//                 end            : 45480893,
    genome: 'tomato',
    plugins: ['controlPanel', 'karyotype', 'trackControls', 'resizer', 'tooltips', 'fileDrop'],
    tracks: [
        Genoverse.Track.Scalebar,
        {
            model: Genoverse.Track.Model.Sequence.Fasta,
            view: Genoverse.Track.View.Sequence,
            autoHeight: false,
            threshold: 30000,
            url: 'cgi-bin/faidx.pl?file=ftp://138.250.31.2/Public/TomatoReference/SL2.50/Sol.2.50.fasta.gz&r=chr__CHR__:__START__-__END__',
            //url: 'cgi-bin/faidx.pl?file=/usr/share/TomatoReference/SL2.50/Sol.2.50.fasta.gz&r=chr__CHR__:__START__-__END__',
        },

        {
            model: Genoverse.Track.Model.Transcript.GFF3,
            name: 'Gene<br/>Models',
            view: Genoverse.Track.View.Transcript,
            url: 'cgi-bin/tabix.pl?file=ftp://138.250.31.2/Public/TomatoSNP/tracks/embrapa/GFF3s/mrna.sorted.gff3.gz&r=chr__CHR__:__START__-__END__',
            labels: true,
            threshold: 10000000,
            intronStyle: 'hat',
            500000: {
                labels: true,
            }
        },
        {
            model: Genoverse.Track.Model.Transcript.GFF3,
            view: Genoverse.Track.View.Transcript,
            name: "SGN<br/>Markers",
            url: 'cgi-bin/tabix.pl?file=ftp://138.250.31.2/Public/TomatoSNP/tracks/embrapa/GFF3s/sgn_markers.sorted.gff3.gz&r=chr__CHR__:__START__-__END__',
            labels: true,
            threshold: 10000000,
            typeMap: {
                exon: 'match_part',
            },
            500000: {
                labels: false,
            }
        },

        Genoverse.Track.SequenceVariation.extend({
            name: 'LA1972<br/>unique<br/>SNPs',
            url: 'cgi-bin/tabix.pl?file=ftp://138.250.31.2/Public/TomatoSNP/tracks/chilbix/LA1972.SNPs.unique.track.vcf.gz&r=chr__CHR__:__START__-__END__',
            threshold: 10000000,
            maxQUAL: 1500,
        }),
        
        Genoverse.Track.SequenceVariation.extend({
            name: 'Proposed<br/>marker<br/>SNPs',
            url: 'cgi-bin/tabix.pl?file=ftp://138.250.31.2/Public/TomatoSNP/tracks/chilbix/LA1972.markers.track.vcf.gz&r=chr__CHR__:__START__-__END__',
            threshold: 100000000,
            maxQUAL: 1500,
        }),
    ]
}