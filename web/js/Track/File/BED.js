Genoverse.Track.File.BED = Genoverse.Track.extend({
  model : Genoverse.Track.Model.Annotation.BED,
  bump  : true,
  featureHeight: 6,
});