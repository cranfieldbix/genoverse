Genoverse.Track.File.GFF = Genoverse.Track.extend({
  model  : Genoverse.Track.Model.Annotation.GFF,
  bump   : true,
  height : 100,
  featureHeight : 5,
});

Genoverse.Track.File.GTF = Genoverse.Track.File.GFF;
