Genoverse.Track.File.GFF3 = Genoverse.Track.extend({
  model : Genoverse.Track.Model.Transcript.GFF3,
  view  : Genoverse.Track.View.Transcript,
  bump  : true,
});